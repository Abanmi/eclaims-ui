﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Statistics.Models
{
    public class getStatistics
    {
            public int number_of_cliams { get; }
            public double amount_claimed_sum { get; }
            public double amount_approved_sum { get; }
            public int paid_amount_sum { get; }
            public int total_in_patient_days_sum { get; }
            public double avg_in_patient_days_sum { get; }
            public double get_average_length_process { get; }
            public double average_percentage_approved_amounts { get; }
    }
}